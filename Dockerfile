FROM openjdk:11
WORKDIR /workspace
COPY target/docker-app*.jar app.jar
EXPOSE 9966
ENTRYPOINT ["java", "-jar", "/workspace/app.jar"]
