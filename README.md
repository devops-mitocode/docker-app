
# Microservicio Jenkins

Se realizo un Jenkinsfile que cumple con las siguientes caracteristicas.



## Descripción

 - Utilizar una aplicación que disponga hecha en Maven que genere un artefacto de
tipo FAT JAR. Se recomienda usar Spring Boot.
 - Crear un repositorio bitbucket público y alojar la aplicación.
 - Crear un pipeline(Jenkinsfile) con los siguientes stages :
    
    - Build
    - Testing(Junit + Jacoco)
    - Sonar (cobertura mayor a 60%)
    - Despliegue en Dockerhub (Use un Dockerfile)  [Dockerhub@eduardo05](https://hub.docker.com/repository/docker/eduardo05/docker-app)


## Feedback

Si tiene un comentario se puede cominicar a  epadilla@apilink.com.mx


## 🚀 Autor
- [@eduardo-padilla-cruz](https://www.linkedin.com/in/eduardo-padilla-cruz)

